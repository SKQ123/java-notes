一、git下载安装

、访问git官方下载网址，点击[此处](https://git-scm.com/downloads)，然后根据自己的电脑系统，下载对应的安装包：（**然后一直next）**

1、GitHub或Gitee官网上注册一个账号，注册好后，桌面右键选择Git Bash，进行账号配置，命令如下：

```bash
1 
2 git config --global user.name "username"[label](c:/Users/Administrator/Desktop/docker%E8%AE%B2%E8%A7%A3.xmap)
3 
4 git config --global user.email "username@email.com"

```

执行完以上命令后，可用git config --global --list查看是否配置成功。

2、继续，执行: [ssh-keygen](https://so.csdn.net/so/search?q=ssh-keygen&spm=1001.2101.3001.7020) -t rsa ，(注意ssh-keygen无空格)，生成SSH(你的电脑与Gitee通信的安全连接)

```bash
1 
2 ssh-keygen -t rsa
3  
4 

```

3、执行完后到系统盘users目录(win: C:\\Users\\你的用户名.ssh\\），查看生成的ssh文件：  
![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/5290dda2dd89457ea3d4e0984a7547c2.png)

4、将[公钥](https://so.csdn.net/so/search?q=%E5%85%AC%E9%92%A5&spm=1001.2101.3001.7020)（ id_rsa.pub），添加到Github或Gitee平台中，这里以Gitee为例：  
![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/1c5f0b82c9914914b5fbbb4331142253.png)

(1)gitee登录成功后，点击个人账户、找到设置，点击SSH公钥；

(2)将公钥（ id_rsa.pub）文件中的内容复制到key中，title随便起，或者key复制完后会自动生成title,点击确定就配置完成啦。  
(3)回到Git bash命令窗口，输入 ssh -t git@gitee.com，如下所示，说明配置成功！  
![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/a7f6881bc9294ba481434dea71b5f8fb.png)

(4)如果执行上条命令，出现了下图情况：  
![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/1a38c7a98052483dac15b6a857d5f221.png)

说明.ssh文件夹下缺少known_hosts文件  
![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/c884d141a5174be9b1fb8c41447aa3ec.png)

输入yes,就可以了  
![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/4fb36ee681574c55846b9cdf5bf930c6.png)