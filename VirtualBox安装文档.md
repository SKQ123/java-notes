### 一、下载Centos服务器版

        **CentOS服务器版本：CentOS-7-x86\_64-Minimal-1810     918M**

        下载一：可以CentOS（本课程使用的 Linux 发行版）官网：[https://www.centos.org](https://www.centos.org/ "https://www.centos.org") 进行下载Centos服务器

        下载二：网盘链接分享: [百度网盘 请输入提取码百度网盘为您提供文件的网络备份、同步和分享服务。空间大、速度快、安全稳固，支持教育网加速，支持手机端。注册使用百度网盘即可享受免费存储空间![](https://pan.baidu.com/m-static/base/static/images/favicon.ico)https://pan.baidu.com/s/1rzmnpMkppTa7pSDFWA0XHA](https://pan.baidu.com/s/1rzmnpMkppTa7pSDFWA0XHA "百度网盘 请输入提取码")         提取码: 2qsd

        网盘有备份

### 二、VirtualBox（VMware）虚拟机新建虚拟服务器

**第1步：打开VirtualBox虚拟机，点击新建**

![](![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/07d67bd9822e4dcf838a301d32c90ed1.png))

 **第2步：新建虚拟电脑**，名称随意填（自己认识即可），文件夹创建新的空文件夹。类型默认选择linux的。

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/da3c3b46810e468a81935ff9a9087bf6.png)

**第3步：设置内存大小和CPU数量**。我这地方分配的是2G内存，3核CPU。大家自行根据自己电脑进行配置。 

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/59287497c5c444afbb720da98731f421.png)

 **第4步：硬盘大小设置。**完成配置后，点击Next。我这边设置15G，10G也可以。可根据自己硬盘情况调整。

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/e906e54f60d74337a42d74bc9b43e7c9.png)

 **第五步：点击点击Finish完成即可**。

接着我们就可以在VirtualBox管理器上看到刚刚安装的虚拟电脑Centos-Server。如栏目中的小红帽图标。

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/969d70ba0ef0472693d4160ba88f5fc1.png)

### 三、VirtualBox（VMware）虚拟机中安装Centos服务器

**第1步：在安装之前，需要先添加盘片。**点击存储——没有盘片——再点击右上角的 类似光盘的小图标，再点击 “选择虚拟光盘”，将之前下载的Centos服务器文件（**CentOS-7-x86\_64-Minimal-1810**）添加进来即可。

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/43b27fbba0a94ea688d4c3466fd4a7d5.png)

 ![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/d42dc7c3ae304295aaf61ae13e1fe7c9.png)

 添加完成如下图：从之前的 没有盘片 变成刚刚添加的 Centos服务器文件。 添加完成后点击OK即可。

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/9315d9cb7c42447b90272809c4105787.png)

 **第2步：接着，启动虚拟机**，点击启动即可（这地方需要稍等一会，系统正在做程序校验）

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/3e8b1bd8e9244ec98340e3c1b6af534f.png)

 **第3步：后面的Centos服务器的安装和Centos桌面版的安装类似，可直接参考Centos桌面版安装步骤。**[VirtualBox虚拟机安装Centos7详细教程图解\_virtualbox安装centos教程\_Steven灬的博客-CSDN博客](https://blog.csdn.net/weixin_40547993/article/details/128934520 "VirtualBox虚拟机安装Centos7详细教程图解_virtualbox安装centos教程_Steven灬的博客-CSDN博客")

以下各功能配置，直接点开进入后，都点击左上角的Done即可。

注：服务器版本没有桌面管理器，进入之后使黑底白字的原始界面。

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/108d3d3bc945461dbf09b98fa4cf6502.png)

 **第4步：配置完成后，点击begin installation开始安装。**安装过程中配置ROOT密码。个人用户密码可创建也可以不用创建，后续直接使用root账户进行登录。**我这边没有创建。**

安装配置完成后，右下角会出现Reboot，即重新启动。

 ![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/8da3dab786a842f590e13bfadb0c9133.png)

**第5步**，点击reboot之后，我们即可进入到Centos服务器(没有桌面)，输入root密码，以root身份登入进去后，可以敲我们之前学习的命令。

**如果想退出，即可使用poweroff命令来关闭服务器退出即可。**

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/d1d841bebe7547fe9913ac11010dbfe6.png)

 以上VirtualBox（VMware）虚拟机安装Centos服务器安装完成

### 四、CentOS服务器的基本配置 

 首先将**默认的网络方式 NAT 改为 桥接网卡:**

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/045bb06c775d4033bb3e0a012b33f5c9.png)

CentOS服务器按照完成后，进入到系统中（黑底白字的原始界面），输入命令hostname -I，如果显示有IP即为正常，使用Xshell远程连接终端即可。

**一定要确保显示有ip，否则无法联网，无法使用yum命令进行软件安装，也无法进行远程连接**

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/9ae4a53b06f74198865f1a04e0a07a05.png)

 如果没有显示IP，即需要将系统网络配置文件/etc/sysconfig/network-scripts/ifcfg-enp0s3中 ONBOOT=NO，修改为**ONBOOT=yes**即可。

首先浏览该文件 /etc/sysconfig/network-scripts/ifcfg-enp0s3，确认一下是否是ONBOOT=NO关闭状态（因为有的系统默认是yes），使用命令  cat  /etc/sysconfig/network-scripts/ifcfg-enp0s3；

**注意：**如果显示没有这个文件，确认下是否敲错。如果没有敲错，**执行cd /etc/sysconfig/network-scripts命令**，进入到network-scripts目录下，接着**执行命令 ls**，查看当前目录下的文件，可有与ifcfg-enp0s3类似的文件；因为**有的系统中网络连接方式不一样，这个配置文件名称就不一样**，例如是ifcfg-enpss3文件或者ifcfg-ens33等其他文件，但是名称差异不大，打开查看一下就清楚了。

下图是我已改过后截的图，刚打开时ONBOOT=NO，改为yes即可：

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/41cfaa8bbdb84173bfe00dd8d30c8aec.png)

 怎么进入该文档改为yes呢？因为没有IP，无法联网，也不能安装nano。具体执行步骤如下：

        **第1步：**执行命令**vi   /etc/sysconfig/network-scripts/ifcfg-enp0s3**

        **第2步：**通过键盘上的上下键，定位到要修改的地方；接着在键盘输入 i 或者 a 或者 o 键都可以**进入编辑状态，将**ONBOOT=NO，修改为**ONBOOT=yes即可**；

        **第3步：**修改完成后，**按键盘左上角esc键，退出编辑**；

        **第4步：**同时按键盘上的 **shift+；键**，此时最下面有个：在闪烁，**输入wq**（即写入退出的意思），再按回车键即可。

此时，网络配置文件编辑并修改完成。

修改配置后，**重启网络：systemctl restart network  或者重启一下**

**此时进入到终端，再输入hostname -I（大写的I）看看是否显示ip，或者执行命令ip addr看看是否显示ip。或者执行命令ping baidu.com，查看是否能够ping通**（按ctrl+c退出连接）**。**

**如果显示ip即为成功。**

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/f6f2b25e40ef4a1293eebb26f21c322a.png)

 如果要使用ifconfig命令查看ip，需要安装net-tools工具包，执行命令：yum install net-tools

![](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/4f649b43a37345349c6fd2cac8e03809.png)

 系统中也没有nano编辑器，可以安装一下，执行命令 **yum install nano**

以上，我们的CentOS服务器配置就配置完成了。

