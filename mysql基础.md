## MYSQL基础

### 1. SQL分类

* DDL(Data Definition Language) ： 数据定义语言，用来定义数据库对象：数据库，表，列等
* DML(Data Manipulation Language) 数据操作语言，用来对数据库中表的数据进行增删改
* DQL(Data Query Language) 数据查询语言，用来查询数据库中表的记录(数据)
* DCL(Data Control Language) 数据控制语言，用来定义数据库的访问权限和安全级别，及创建用户
> 注意： 以后我们最常操作的是 `DML` 和 `DQL`  ，因为我们开发中最常操作的就是数据。

## 2. sql查询

### 2.1 数据库操作

```sql
SHOW DATABASES; //查询数据库
CREATE DATABASE 数据库名称;//创建数据库
CREATE DATABASE IF NOT EXISTS 数据库名称;
DROP DATABASE 数据库名称;//删除数据库
DROP DATABASE IF EXISTS 数据库名称;
USE 数据库名称;//使用数据库
SELECT DATABASE();//查看当前使用的数据库
```

### 2.2 表基础操作

操作表也就是对表进行增（Create）删（Retrieve）改（Update）查（Delete）。

```sql
SHOW TABLES;//查询表
DESC 表名称;//查询表结构
CREATE TABLE 表名 (
	字段名1  数据类型1,
	字段名2  数据类型2,
	…
	字段名n  数据类型n
);//创建表
DROP TABLE 表名;//删除表



```
> 注意：创建表最后一行末尾，不能加逗号


### 2.3  数据类型

MySQL 支持多种类型，可以分为三类：

* 数值

  ```sql
  tinyint : 小整数型，占一个字节
  int	： 大整数类型，占四个字节
  	eg ： age int
  double ： 浮点类型
  	使用格式： 字段名 double(总长度,小数点后保留的位数)
  	eg ： score double(5,2)   
  ```

* 日期

  ```sql
  date ： 日期值。只包含年月日
  	eg ：birthday date ： 
  datetime ： 混合日期和时间值。包含年月日时分秒
  ```

* 字符串

  ```sql
  char ： 定长字符串。
  	优点：存储性能高
  	缺点：浪费空间
  	eg ： name char(10)  如果存储的数据字符个数不足10个，也会占10个的空间
  varchar ： 变长字符串。
  	优点：节约空间
  	缺点：存储性能底
  	eg ： name varchar(10) 如果存储的数据字符个数不足10个，那就数据字符个数是几就占几个的空间	
  ```


### 2.4  表数据操作


```sql
ALTER TABLE 表名 RENAME TO 新的表名;//修改表名
ALTER TABLE 表名 ADD 列名 数据类型;//添加一列
ALTER TABLE 表名 MODIFY 列名 新数据类型;//修改数据类型
ALTER TABLE 表名 CHANGE 列名 新列名 新数据类型;//修改列名和数据类型
ALTER TABLE 表名 DROP 列名;//删除列
```



## 2.5 DML(增删改操作)

DML主要是对数据进行增（insert）删（delete）改（update）操作。

### 2.5.1  添加数据

```sql
INSERT INTO 表名(列名1,列名2,…) VALUES(值1,值2,…);//给指定列添加数据
INSERT INTO 表名 VALUES(值1,值2,…);//给全部列添加数据

INSERT INTO 表名(列名1,列名2,…) VALUES(值1,值2,…),(值1,值2,…),(值1,值2,…)…;//批量添加数据
INSERT INTO 表名 VALUES(值1,值2,…),(值1,值2,…),(值1,值2,…)…;//批量添加数据
```


### 2.5.2 修改数据

```sql
UPDATE 表名 SET 列名1=值1,列名2=值2,… [WHERE 条件] ;//修改表数据 [WHERE 条件]可选
```
> 注意：
>
> 1. 修改语句中如果不加条件，则将所有数据都修改！
> 2. 像上面的语句中的中括号，表示在写sql语句中可以省略这部分


### 2.5.3  删除数据

```sql
DELETE FROM 表名 [WHERE 条件] ;//删除数据 [WHERE 条件]可选
```

## 3. DQL 查询语句

查询的完整语法：

```sql
SELECT 
    字段列表
FROM 
    表名列表 
WHERE 
    条件列表
GROUP BY
    分组字段
HAVING
    分组后条件
ORDER BY
    排序字段
LIMIT
    分页限定
```


### 2.6  基础查询

#### 2.6.1  语法

* **查询多个字段**

```sql
SELECT 字段列表 FROM 表名;
SELECT * FROM 表名; //查询所有数据
SELECT DISTINCT 字段列表 FROM 表名;//去除重复记录
AS: // 起别名，AS 也可以省略
```

### 2.7  条件查询

#### 2.7.1  语法

```sql
SELECT 字段列表 FROM 表名 WHERE 条件列表;
```

* **条件**

条件列表可以使用以下运算符

<img src="assets/image-20210722190508272.png" alt="image-20210722190508272" style="zoom:60%;" />

#### 2.7.2  条件查询练习

* 查询年龄大于20岁的学员信息

  ```sql
  select * from stu where age > 20;//查询年龄大于20岁的学员信息
  select * from stu where age >= 20;//查询年龄大于等于20岁的学员信息
  select * from stu where age >= 20 &&  age <= 30;//查询年龄>=20岁且<=30岁 的学员
  select * from stu where age >= 20 and  age <= 30;//查询年龄>=20岁且<=30岁 的学员
  select * from stu where age BETWEEN 20 and 30;//查询年龄>=20岁且<=30岁 的学员
  //查询年龄不等于18岁的学员信息
  select * from stu where age != 18;//查询年龄不等于18岁的学员信息
  select * from stu where age <> 18;
  //查询英语成绩为 null的学员信息
  select * from stu where english is null;
  select * from stu where english is not null;

  ```

#### 2.7.3  模糊查询练习

> 模糊查询使用like关键字，可以使用通配符进行占位:
> （1）_ : 代表单个任意字符
> （2）% : 代表任意个数字符


  ```sql
  //查询姓'马'的学员信息
  select * from stu where name like '马%';
  ```
* 查询第二个字是'花'的学员信息  

  ```sql
  select * from stu where name like '_花%';
  ```
* 查询名字中包含 '德' 的学员信息
  ```sql
  select * from stu where name like '%德%';
  ```

  

### 2.8  排序查询

#### 2.8.1  语法

```sql
SELECT 字段列表 FROM 表名 ORDER BY 排序字段名1 [排序方式1],排序字段名2 [排序方式2] …;
```

上述语句中的排序方式有两种，分别是：

* ASC ： 升序排列 **（默认值）**
* DESC ： 降序排列

> 注意：如果有多个排序条件，当前边的条件值一样时，才会根据第二条件进行排序

#### 2.8.2  练习

* 查询学生信息，按照年龄升序排列 

  ```sql
  select * from stu order by age ;
  ```

* 查询学生信息，按照数学成绩降序排列

  ```sql
  select * from stu order by math desc ;
  ```

* 查询学生信息，按照数学成绩降序排列，如果数学成绩一样，再按照英语成绩升序排列

  ```sql
  select * from stu order by math desc , english asc ;
  ```


### 2.9  聚合函数

#### 2.9.1  概念

 ==将一列数据作为一个整体，进行纵向计算。==

如何理解呢？假设有如下表

<img src="assets/image-20210722194410628.png" alt="image-20210722194410628" style="zoom:80%;" />

现有一需求让我们求表中所有数据的数学成绩的总和。这就是对math字段进行纵向求和。

#### 2.9.2  聚合函数分类

| 函数名      | 功能                             |
| ----------- | -------------------------------- |
| count(列名) | 统计数量（一般选用不为null的列） |
| max(列名)   | 最大值                           |
| min(列名)   | 最小值                           |
| sum(列名)   | 求和                             |
| avg(列名)   | 平均值                           |

#### 2.9.3  聚合函数语法

```sql
SELECT 聚合函数名(列名) FROM 表;
```

> 注意：null 值不参与所有聚合函数运算



#### 2.9.4  练习

* 统计班级一共有多少个学生

  ```sql
  select count(id) from stu;
  select count(english) from stu;
  ```

  上面语句根据某个字段进行统计，如果该字段某一行的值为null的话，将不会被统计。所以可以在count(*) 来实现。\* 表示所有字段数据，一行中也不可能所有的数据都为null，所以建议使用 count(\*)

  ```sql
  select count(*) from stu;
  ```

* 查询数学成绩的最高分

  ```sql
  select max(math) from stu;
  ```

* 查询数学成绩的最低分

  ```sql
  select min(math) from stu;
  ```

* 查询数学成绩的总分

  ```sql
  select sum(math) from stu;
  ```

* 查询数学成绩的平均分

  ```sql
  select avg(math) from stu;
  ```

* 查询英语成绩的最低分

  ```sql
  select min(english) from stu;
  ```

  

### 2.10  分组查询

#### 2.10.1  语法

```sql
SELECT 字段列表 FROM 表名 [WHERE 分组前条件限定] GROUP BY 分组字段名 [HAVING 分组后条件过滤];
```

> 注意：分组之后，查询的字段为聚合函数和分组字段，查询其他字段无任何意义



#### 2.10.2  练习

* 查询男同学和女同学各自的数学平均分

  ```sql
  select sex, avg(math) from stu group by sex;
  ```

  > 注意：分组之后，查询的字段为聚合函数和分组字段，查询其他字段无任何意义

  ```sql
  select name, sex, avg(math) from stu group by sex;  -- 这里查询name字段就没有任何意义
  ```

* 查询男同学和女同学各自的数学平均分，以及各自人数

  ```sql
  select sex, avg(math),count(*) from stu group by sex;
  ```

* 查询男同学和女同学各自的数学平均分，以及各自人数，要求：分数低于70分的不参与分组

  ```sql
  select sex, avg(math),count(*) from stu where math > 70 group by sex;
  ```

* 查询男同学和女同学各自的数学平均分，以及各自人数，要求：分数低于70分的不参与分组，分组之后人数大于2个的

  ```sql
  select sex, avg(math),count(*) from stu where math > 70 group by sex having count(*)  > 2;
  ```

  

**where 和 having 区别：**

* 执行时机不一样：where 是分组之前进行限定，不满足where条件，则不参与分组，而having是分组之后对结果进行过滤。

* 可判断的条件不一样：where 不能对聚合函数进行判断，having 可以。



### 2.11  分页查询

如下图所示，大家在很多网站都见过类似的效果，如京东、百度、淘宝等。分页查询是将数据一页一页的展示给用户看，用户也可以通过点击查看下一页的数据。

<img src="assets/image-20210722230330366.png" alt="image-20210722230330366" style="zoom:80%;" />

接下来我们先说分页查询的语法。

#### 2.11.1  语法

```sql
SELECT 字段列表 FROM 表名 LIMIT  起始索引 , 查询条目数;
```

> 注意： 上述语句中的起始索引是从0开始



#### 2.11.2  练习

* 从0开始查询，查询3条数据

  ```sql
  select * from stu limit 0 , 3;
  ```

* 每页显示3条数据，查询第1页数据

  ```sql
  select * from stu limit 0 , 3;
  ```

* 每页显示3条数据，查询第2页数据

  ```sql
  select * from stu limit 3 , 3;
  ```

* 每页显示3条数据，查询第3页数据

  ```sql
  select * from stu limit 6 , 3;
  ```

从上面的练习推导出起始索引计算公式：

```sql
起始索引 = (当前页码 - 1) * 每页显示的条数
```

