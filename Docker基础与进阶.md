# Docker

## 一、基础篇

### 1.Docker简介

更快速的应用交付和部署

更便捷的升级和扩缩容

更简单的系统运维

更高效的计算资源利用



docker官网：http://www.docker.com

Docker Hub官网: https://hub.docker.com/

![image-20230907105009776](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/image-20230907105009776.png)



### 2.Docker安装

#### 前提条件

CentOS Docker 安装
目前，CentOS 仅发行版本中的内核支持 Docker。Docker 运行在CentOS 7 (64-bit)上，
要求系统为64位、Linux系统内核版本为 3.8以上，这里选用Centos7.x

查看自己的内核
uname命令用于打印当前系统相关信息（内核版本号、硬件架构、主机名称和操作系统类型等）。

#### Docker平台架构图解(入门版)：

![image-20230907105343028](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/image-20230907105343028.png)



Docker平台架构图解(架构版)：

![image-20230907105619497](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/image-20230907105619497.png)



安装步骤：

#### 2.1确定你是CentOS7及以上版本

​	cat /etc/redhat-release

#### 2.2卸载旧版本

教程网址：https://docs.docker.com/engine/install/centos/

![image-20230907110006584](https://gitee.com/SKQ123/pushPictures/raw/master/pictures/image-20230907110006584.png)

#### 2.3yum安装gcc相关

yum -y install gcc

yum -y install gcc-c++

#### 2.4安装需要的软件包

yum install -y yum-utils

#### 2.5设置stable镜像仓库

使用阿里云镜像安装仓库：yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

#### 2.6更新yum软件包索引

yum makecache fast

#### 2.7安装DOCKER CE

yum -y install docker-ce docker-ce-cli containerd.io

#### 2.8启动docker

systemctl start docker

#### 2.9测试

docker version

docker run hello-world

#### 2.10卸载

systemctl stop docker 

yum remove docker-ce docker-ce-cli containerd.io

rm -rf /var/lib/docker 

rm -rf /var/lib/containerd





添加阿里云镜像容器来加速：

```
获取加速链接：
登陆阿里云开发者平台
点击控制台
选择容器镜像服务
获取加速器地址
```



```
粘贴脚本直接执行：
mkdir -p /etc/docker
vim  /etc/docker/daemon.json

脚本如下：
#阿里云
{
  "registry-mirrors": ["https://｛自已的编码｝.mirror.aliyuncs.com"]
}
```



重载脚本和重启docker：

```
systemctl daemon-reload
systemctl restart docker
```

### 3.Docker常用命令

1. ```
   启动docker： systemctl start docker
   
   停止docker： systemctl stop docker
   
   重启docker： systemctl restart docker
   
   查看docker状态： systemctl status docker
   
   开机启动： systemctl enable docker
   
   查看docker概要信息： docker info
   
   查看docker总体帮助文档： docker --help
   
   查看docker命令帮助文档： docker 具体命令 --help
   查看docker的镜像 docker images -a :列出本地所有的镜像（含历史映像层）
                    docker images -q :只显示镜像ID。
                    
   docker search [OPTIONS] 镜像名字   
   eq：·--limit : 只列出N个镜像，默认25个
   docker search --limit 5 redis
   
   
   docker pull 镜像名字：TAG
   eq:
   docker pull redis:6.0.8
   
   查看镜像/容器/数据卷所占的空间 docker system df 
   删除镜像：docker rmi 某个XXX镜像名字ID
   
   
   启动前台容器或者后台容器：docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
   
   OPTIONS说明（常用）：有些是一个减号，有些是两个减号
   
   --name="容器新名字" 为容器指定一个名称；
   -d: 后台运行容器并返回容器ID，也即启动守护式容器(后台运行)；
    
   -i：以交互模式运行容器，通常与 -t 同时使用；
   -t：为容器重新分配一个伪输入终端，通常与 -i 同时使用；
   也即启动交互式容器(前台有伪终端，等待交互)；
    
   -P: 随机端口映射，大写P
   -p: 指定端口映射，小写p
   
   
   启动centos镜像：
   
   ·前台交互式启动
   · docker run -it redis:6.0.8
   docker run -it centos /bin/bash 
   ·后台守护式启动
   ·docker run -d redis:6.0.8
   docker run -d centos /bin/bash
   退出容器：
   
   exit退出，容器停止
   ctrl+p+q退出，容器不停止
   
   
   ·启动容器
   docker start 容器ID或者容器名
   ·重启容器
   docker restart 容器ID或者容器名
   ·停止容器
   docker stop 容器ID或者容器名
   ·强制停止容器
   docker kill 容器ID或容器名
   ·删除已停止的容器
   docker rm 容器ID
   ·一次性删除多个容器实例
   docker rm -f $(docker ps -a -q)
   docker ps -a -q | xargs docker rm
   ```







![image-20230907134500141](Docker基础与进阶.assets/image-20230907134500141.png)

REPOSITORY：表示镜像的仓库源

TAG：镜像的标签版本号

IMAGE ID：镜像ID

CREATED：镜像创建时间

SIZE：镜像大小



















